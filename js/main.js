(function ($) {

    "use strict";
    new WOW().init();


    //preloader

  let  mainStatus  = $('.preloader-status');
   let mainBody = $('body');
   let mainPreloader  = $('.preloader');
   window.onload = function() {
    mainStatus.fadeOut();
    mainPreloader.delay(500).fadeOut('slow');
    mainBody.addClass('loaded').delay(500).css({
        'overflow': 'auto'
    });

  }

  // language toggle

 /* $(".english-language").click(function () {
    $("body").addClass("english-version");
    
  });

  $(".arabic-language").click(function () {
    $("body").removeClass("english-version");
  })*/
  

    // auto complete
    $(".form-control").attr("autocomplete","off");


   // custom file input


   $(document).ready(function () {
    bsCustomFileInput.init()
  })

  $("input[type=file]").on('change',function(event){

    let fileVal=event.target.value;
    
    if(fileVal){

      $(this).next(".custom-file-label").addClass('colored');


    }
    else{

      $(this).next(".custom-file-label").removeClass('colored');

    }
  })

   // modal
   
   $(document).on('show.bs.modal', function (event) {
    if (!event.relatedTarget) {
        $('.modal').not(event.target).modal('hide');
    
    };
    if ($(event.relatedTarget).parents('.modal').length > 0)
     {
        $(event.relatedTarget).parents('.modal').modal('hide');
       
    };
    $('body').addClass('modal-open');
    mainBody.addClass('loaded').css({
      'overflow': 'hidden'
  });
  });

  $(document).on('shown.bs.modal', function () {

    if ($('body').hasClass('modal-open') == false) {
        $('body').addClass('modal-open');
        mainBody.addClass('loaded').css({
          'overflow': 'hidden'
      });
        
    };
   
    
  });

  $(document).on('hidden.bs.modal', function () {
  
    mainBody.addClass('loaded').css({
      'overflow': 'auto'
  });
        
  });


   // effect mouseenter && mouseout

   $(".btn,.nav-link , .tabs-link , .social-list a").on('mouseenter', function(e) {
    let parentOffset = $(this).offset(),
       relX = e.pageX - parentOffset.left,
       relY = e.pageY - parentOffset.top;
       $(this).find('span').not(".text,.icon").css({top:relY, left:relX})
    })
    .on('mouseout', function(e) {
     let parentOffset = $(this).offset(),
       relX = e.pageX - parentOffset.left,
       relY = e.pageY - parentOffset.top;
       $(this).find("span").not(".text,.icon").css({top:relY, left:relX})
    })
    

    //responsive navbar

let navbarcollapse=$(".navbar-light .navbar-collapse");
let navitemcollpase=$(".navbar-light .navbar-nav>.nav-item");
let delay=0;
$(".nav-toggler").click(function(){

  $(this).toggleClass("toggle");
   

  if($(this).hasClass("toggle")){

    $(navbarcollapse).addClass("open");

    $(navitemcollpase).each(function () {
      let $navitem=$(this);
      setTimeout(function () {
          $navitem.addClass("fade");
      }, delay += 40);
    });

    $("body").addClass('hide-flow');
  }

  else{

    $(navbarcollapse).removeClass("open");
    $(navitemcollpase).each(function () {
      let $navitem=$(this);
      setTimeout(function () {
          $navitem.removeClass("fade");
      }, delay -= 40);
    });

    $("body").removeClass('hide-flow');

  }

});


    $(window).resize(function(){
    if ($(window).width() <= 991){ 

      $(".nav-toggler").removeClass("toggle");
      $(navbarcollapse).removeClass("open");
      $(navitemcollpase).removeClass("fade");
      $("body").removeClass('hide-flow');
    }     
});




  //scroll
  $(document).ready(function() {
  
    var scrollLink = $('.scroll');
    
    // Smooth scrolling
    scrollLink.click(function(e) {
      e.preventDefault();
      $('body,html').animate({
        scrollTop: $(this.hash).offset().top
      }, 1000 );
    });
    
    // Active link switching
    $(window).scroll(function() {
      var scrollbarLocation = $(this).scrollTop();
      
      scrollLink.each(function(e) {
        
        var sectionOffset = $(this.hash).offset().top - 100;
        
        if ( sectionOffset <= scrollbarLocation ) {
          $(this).parent().addClass('active');
          $(this).parent().siblings().removeClass('active');
          
        }
      })
      
    })
    
  })
  //sticky navbar   

  $(window).scroll(function () {


    if ($(window).scrollTop()) {
        $('.navbar-light').addClass('sticky-top').animate({
  
        }, 4000);
  
    } 
    else {
        $('.navbar-light').removeClass('sticky-top').animate({
  
        }, 4000);
  
      
    }
  
  });


  //common questions tabs

  $(".tabs li").click(function () {
    $(this).addClass("active-tabs").siblings().removeClass("active-tabs");
     });

  
     
   $('.tabs').find('> li:eq(0)').addClass('current');
   
   $('.tabs li a').click(function (e) { 
     var tab = $(this).closest('.tab'), 
       index = $(this).closest('li').index();
     
     tab.find('.tabs > li').removeClass('current');
     $(this).closest('li').addClass('current');
     
     tab.find('.tab-content').find('div.tabs-item').not('div.tabs-item:eq(' + index + ')').slideUp();
     tab.find('.tab-content').find('div.tabs-item:eq(' + index + ')').slideDown();
     
     e.preventDefault();
   });


   // tabs collapse
   
  if($(".collapse").hasClass("show")){

    $(".collapse").parents(".card").find(".card-icon").addClass("rotate");
  }
  else{

    $(".collapse").parents(".card").find(".card-icon").removeClass("rotate");

  }
   $(".collapse").on('shown.bs.collapse', function () {

    $(this).parents(".card").find(".card-icon").addClass("rotate");
  })

  $('.collapse').on('hidden.bs.collapse', function () {
    
    $(this).parents(".card").find(".card-icon").removeClass("rotate");
  })


  
    // scroll to top
    
    $(window).scroll(function(){
      if($(this).scrollTop() > 300) {
    
        $(".auto-scroll-to-top").removeClass("non-hover");                 
        $(".auto-scroll-to-top").addClass("visible");  

       } 
       else {

        $(".auto-scroll-to-top").addClass("non-hover");
        $(".auto-scroll-to-top").removeClass("visible");

       }    

      });
          
     $('.auto-scroll-to-top').on('click touchend', function(event) {
      $("html, body").animate({scrollTop: 0}, 1000);
      event.preventDefault();
    });


})(jQuery);
